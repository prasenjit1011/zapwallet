@extends('layouts.layout')

@section('content')
<div class="wrapper">
    <div class="header header-filter" style="background-image: url('{{ url('/img') }}/city.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="brand">
                        <h1>Zap Wallet.</h1>
                        <h3>Bus Search</h3>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="main main-raised">
        <div class="section section-basic">
            <div class="container">
                <div class="col-sm-12">
                    <form method="POST" action="{{ route('busSearch') }}">
                        @csrf
                            <div class="col-sm-6">
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label" style="text-transform: capitalize;">Starting City (*)</label>
                                        <input type="text" name="from_city" id="from_city"  class="form-control"  required="required" value="{{ $from_city ?? '' }}">
                                        <input type="hidden" name="from_city_id" id="from_city_id" value="{{ $from_city_id ?? '' }}">

                                        <span class="material-input"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label" style="text-transform: capitalize;">Destination City (*)</label>
                                        <input type="text" name="to_city" id="to_city"  class="form-control" required="required"  value="{{ $to_city ?? ''}}">
                                        <input type="hidden" name="to_city_id" id="to_city_id" value="{{ $to_city_id ?? '' }}">
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label" style="text-transform: capitalize;">Departure Date (*)</label>
                                        <input type="text" name="d_date" id="d_date"  class="form-control" required="required" value="{{ $d_date }}">
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label" style="text-transform: capitalize;">Return Date (optional)</label>
                                        <input type="text" name="r_date" id="r_date"  class="form-control"  value="{{ $r_date }}">
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-sm-offset-4">
								<button class="btn btn-success btn-lg" style="width: 100%; margin-top: 20px;">Search Bus</button>
                            </div>
                    </form>
                </div>
				
				
                <div class="col-sm-12">
                    <h2>Result</h2>
					<?php if(isset($myurl)){echo $myurl.'<hr>';}?>
					
					@if(isset($result->data->Buses))
						<table width="100%" style="margin-bottom:30px;">
							<thead>
								<th>Operator</th>
								<th>Trust Score</th>
								<th>Departure</th>
								<th>Duration</th>
								<th>Arrival</th>
								<th>Starting Price</th>
								<th></th>
							</thead>
							<tbody>
								@foreach($result->data->Buses as $val)
									<?php
										$CompanyName 	= '';
										if(isset($val->CompanyName)){
											$CompanyName 	= $val->CompanyName;
										}
										
										$Duration 	= '';
										if(isset($val->Duration)){
											$Duration 	= $val->Duration;
										}
										
										$PickupTime		= '';
										if(isset($val->Pickups[0]->PickupTime)){
											$PickupTime		= $val->Pickups[0]->PickupTime;
										}
										$DropoffTime		= '';
										if(isset($val->Dropoffs[0]->DropoffTime)){
											$DropoffTime		= $val->Dropoffs[0]->DropoffTime;
										}
										
										$DiscountAmt = 0;
										if(isset($val->DiscountAmt)){
											$DiscountAmt 	= $val->DiscountAmt;
										}
										
									?>
									<tr>
										<td>{{$CompanyName}}</td>
										<td></td>
										<td>{{$PickupTime}}</td>
										<td>{{$Duration}}</td>
										<td>{{$DropoffTime}}</td>
										<td>{{$DiscountAmt}}</td>
										<td>
											<input type="button" class="btn btn-sm btn-default" value="Select Seat" onclick="javascript:alert('Please wait work in progress.')" >
											<?php //echo '<pre>';print_r($val);echo '</pre>';?>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						
						
						
					@else
						No Bus Found
					@endif					
					<?php //dd($result->data->Buses);
					/*?>
                    @forelse ($result ?? ''->data->Buses as $bus)
                        <p class="bg-danger text-white p-1">bus</p>
                    @empty
                        <p class="bg-danger text-white p-1">No bus available</p>
                    @endforelse

                    {{-- @php
                        var_dump($result ?? ''->data->Buses);
                    @endphp --}}*/?>
                </div>
            </div>
        </div>
    </div>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
        var path = "{{ route('autocomplete') }}";
        $('#to_city').autocomplete({
            source:  function (query, reponse) {

                $.ajax({
                    url : "{{ route('autocomplete') }}",
                    dataType : 'json',
                    data : {"query": query.term },
                    success : function(donnee){

                        reponse($.map(donnee, function(objet){

                            return {
                                label: objet.city,
                                value: objet.cityId
                                };
                        }));

                    }
                });
            },

            minLength: 3,
            delay:0,

            select: function( event, ui ) {
                $(' #to_city ' ).val( ui.item.label );
                $(' #to_city_id ').val( ui.item.value );
                return false;
            } ,

            messages: {
                noResults: '',
                results: function() {}
            }
        });
        $('#from_city').autocomplete({
            source:  function (query, reponse) {

                $.ajax({
                    url : "{{ route('autocomplete') }}",
                    dataType : 'json',
                    data : {"query": query.term },
                    success : function(donnee){

                        reponse($.map(donnee, function(objet){

                            return {
                                label: objet.city,
                                value: objet.cityId
                                };
                        }));

                    }
                });
            },

            minLength: 3,
            delay:0,

            select: function( event, ui ) {
                $(' #from_city ' ).val( ui.item.label );
                $(' #from_city_id ').val( ui.item.value );
                return false;
            } ,

            messages: {
                noResults: '',
                results: function() {}
            }
        });

        $(function () {
                $('#d_date').datepicker({  dateFormat: "yy-mm-dd", minDate: 0  });
                $('#r_date').datepicker({  dateFormat: "yy-mm-dd" , minDate: 0 });
        });
    </script>





</div>
@endsection
